﻿using System;

namespace AsyncAndAwait
{
    public static class Program
    {
        static readonly ArrayOperations arrayOperations = new(new RandomValueGenerators.RandomValueGenerator());

        public static async Task Main()
        {
            Task<int[]> createArrayTask = CreateArrayTask();
            Console.WriteLine("Control returned after task 1 started");
            
            Task<int[]> multiplyArrayTask = MultiplyArrayTask(await createArrayTask);
            Console.WriteLine("Control returned after task 2 started");

            Task<int[]> sortArrayTask = SortArrayTask(await multiplyArrayTask);
            Console.WriteLine("Control returned after task 3 started");

            Task<double> getArrayAverageTask = GetArrayAverageTask(await sortArrayTask);
            Console.WriteLine("Control returned after task 4 started");

            Console.WriteLine($"Value of last task is array average: {await getArrayAverageTask}");
        }

        private static async Task<int[]> CreateArrayTask()
        {
            int[] array;

            array = await Task<int[]>.Run(() =>
            {
                Task.Delay(2000).Wait();
                return arrayOperations.CreateArray();
            });

            return array;
        }

        private static async Task<int[]> MultiplyArrayTask(int[] array)
        {
            array = await Task<int[]>.Run(() =>
            {
                Task.Delay(2000).Wait();
                return arrayOperations.MultiplyArrayByRandomNumber(array);
            });

            return array;
        }

        private static async Task<int[]> SortArrayTask(int[] array)
        {
            array = await Task.Run<int[]>(() =>
            {
                Task.Delay(2000).Wait();
                return ArrayOperations.SortArrayByAscending(array);
            });

            return array;
        }

        private static async Task<double> GetArrayAverageTask(int[] array)
        {
            double arrayAverage;

            arrayAverage = await Task.Run<double>(() =>
            {
                Task.Delay(2000).Wait();
                return ArrayOperations.GetAverageArrayValue(array);
            });

            return arrayAverage;
        }
    }
}