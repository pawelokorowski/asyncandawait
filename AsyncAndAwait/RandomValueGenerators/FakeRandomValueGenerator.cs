﻿using AsyncAndAwait.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAndAwait.RandomValueGenerators
{
    public class FakeRandomValueGenerator : IRandomValueGenerator
    {
        public int Next()
        {
            return 5;
        }
    }
}
