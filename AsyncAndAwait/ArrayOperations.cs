﻿using AsyncAndAwait.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAndAwait
{
    public class ArrayOperations
    {
        private readonly IRandomValueGenerator arrayElementsGenerator;

        public ArrayOperations(IRandomValueGenerator arrayElementsGenerator)
        {
            if (arrayElementsGenerator is null)
            {
                throw new ArgumentNullException(nameof(arrayElementsGenerator));
            }

            this.arrayElementsGenerator = arrayElementsGenerator;
        }

        public int[] CreateArray()
        {
            var array = new int[10];

            for (int arrayIndex = 0; arrayIndex < array.Length; arrayIndex++)
            {
                array[arrayIndex] = arrayElementsGenerator.Next();
            }

            Console.WriteLine($"Generated Array:");
            PrintArray(array);

            return array;
        }

        public int[] MultiplyArrayByRandomNumber(int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new InvalidOperationException("Array length can not be 0");
            }

            var arrayMultiplicationNumber = arrayElementsGenerator.Next();

            Console.WriteLine($"arrayMultiplicationNumber: {arrayMultiplicationNumber}");

            for (int arrayIndex = 0; arrayIndex < array.Length; arrayIndex++)
            {
                array[arrayIndex] *= arrayMultiplicationNumber;
            }

            Console.WriteLine($"Multiplied array:");
            PrintArray(array);

            return array;
        }

        public static int[] SortArrayByAscending(int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new InvalidOperationException("Array length can not be 0");
            }

            Array.Sort(array);

            Console.WriteLine($"Sorted Array:");
            PrintArray(array);

            return array;
        }

        public static double GetAverageArrayValue(int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new InvalidOperationException("Array length can not be 0");
            }

            double averageArrayValue = array.Average();

            Console.WriteLine($"AverageArrayValue: {averageArrayValue}");

            return averageArrayValue;
        }

        private static void PrintArray(int[] array)
        {
            Console.WriteLine(string.Join(", ", array));
        }
    }
}