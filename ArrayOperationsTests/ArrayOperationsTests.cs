using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using AsyncAndAwait;
using AsyncAndAwait.Interfaces;
using AsyncAndAwait.RandomValueGenerators;

namespace ArrayOperationsTests
{
    [TestFixture]
    public class ArrayOperationsTests
    {
        private ArrayOperations? operations;

        [SetUp]
        public void Initialize()
        {
            var randFake = new FakeRandomValueGenerator();
            operations = new ArrayOperations(randFake);
        }

        [Test]
        public void ArrayOperations_CreateArray_CreatesArray_Of_TenRandomIntegers()
        {
            int[] expectedArray = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
            Assert.That(operations!.CreateArray(), Is.EqualTo(expectedArray));
        }

        [TestCase(new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }, new int[] { 25, 25, 25, 25, 25, 25, 25, 25, 25, 25 })]
        [TestCase(new int[] { 11, 11, 11, 11, 11, 11, 11, 11, 11, 11 }, new int[] { 55, 55, 55, 55, 55, 55, 55, 55, 55, 55 })]
        [TestCase(new int[] { 1, 3, 2, 0 }, new int[] { 5, 15, 10, 0 })]
        [TestCase(new int[] { -1, -2, -11, 6 }, new int[] { -5, -10, -55, 30 })]
        public void ArrayOperations_MultiplyArrayByRandomNumber_Multiplies_ArrayValues_By_RandomNumber(int[] baseArray, int[] expectedArray)
        {
            Assert.That(operations!.MultiplyArrayByRandomNumber(baseArray), Is.EqualTo(expectedArray));
        }

        [TestCase(new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }, new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 })]
        [TestCase(new int[] { 1, 3, 2, 0 }, new int[] { 0, 1, 2, 3 })]
        [TestCase(new int[] { int.MaxValue, 2, -11, int.MinValue }, new int[] { int.MinValue, -11, 2, int.MaxValue })]
        [TestCase(new int[] { 10, 6, 7, 1, 2, 5, 3, 4, 8, 9 }, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })]
        public void ArrayOperations_SortArrayByAscending_Sorts_Array_In_AscendingOrder(int[] baseArray, int[] expectedArray)
        {
            Assert.That(ArrayOperations.SortArrayByAscending(baseArray), Is.EqualTo(expectedArray));
        }

        [TestCase(new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }, 5)]
        [TestCase(new int[] { 1, 3, 2, 0 }, 1.5)]
        [TestCase(new int[] { int.MaxValue, int.MinValue }, -0.5)]
        [TestCase(new int[] { 10, 6, 7, 1, 2, 5, 3, 4, 8, 9 }, 5.5)]
        public void ArrayOperations_GetAverageArrayValue_Returns_AverageValue_Of_ArrayElements(int[] baseArray, double average)
        {

            Assert.That(ArrayOperations.GetAverageArrayValue(baseArray), Is.EqualTo(average));
        }

        [Test]
        public void ArrayOperations_MultiplyArrayByRandomNumber_Throws_ArgumentNullException_When_Array_IsNull()
        {
            Assert.Throws<ArgumentNullException>(() => operations!.MultiplyArrayByRandomNumber(null));
        }

        [Test]
        public void ArrayOperations_SortArrayByAscending_Throws_ArgumentNullException_When_Array_IsNull()
        {
            Assert.Throws<ArgumentNullException>(() => ArrayOperations.SortArrayByAscending(null));
        }

        [Test]
        public void ArrayOperations_GetAverageArrayValue_Throws_ArgumentNullException_When_Array_IsNull()
        {
            Assert.Throws<ArgumentNullException>(() => ArrayOperations.GetAverageArrayValue(null));
        }

        [Test]
        public void ArrayOperations_MultiplyArrayByRandomNumber_Throws_InvalidOperationException_When_Array_IsEmpty()
        {
            Assert.Throws<InvalidOperationException>(() => operations!.MultiplyArrayByRandomNumber(new int[0]));
        }

        [Test]
        public void ArrayOperations_SortArrayByAscending_Throws_InvalidOperationException_When_Array_IsEmpty()
        {
            Assert.Throws<InvalidOperationException>(() => ArrayOperations.SortArrayByAscending(new int[0]));
        }

        [Test]
        public void ArrayOperations_GetAverageArrayValue_Throws_InvalidOperationException_When_Array_IsEmpty()
        {
            Assert.Throws<InvalidOperationException>(() => ArrayOperations.GetAverageArrayValue(new int[0]));
        }

        [Test]
        public void ArrayOperations_MultiplyArrayByRandomNumber_Returns_Reference_To_Original_Array()
        {
            int[] originalArray = new int[] { 10, 6, 7, 1, 2, 5, 3, 4, 8, 9 };
            Assert.That(operations!.MultiplyArrayByRandomNumber(originalArray), Is.SameAs(originalArray));
        }

        [Test]
        public void ArrayOperations_SortArrayByAscending_Returns_Reference_To_Original_Array()
        {
            int[] originalArray = new int[] { 10, 6, 7, 1, 2, 5, 3, 4, 8, 9 };
            Assert.That(ArrayOperations.SortArrayByAscending(originalArray), Is.SameAs(originalArray));
        }
    }
}